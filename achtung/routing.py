from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/achtung/(?P<room_name>[^/]+)/(?P<nickname>[^/]+)/$', consumers.GameControlConsumer),
    # url(r'^ws/achtung/car/$', consumers.GameControlConsumer),
]
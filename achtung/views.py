from django.shortcuts import render
from django.utils.safestring import mark_safe
import json

def index(request, room_name=None):
    if request.method == "GET":
        return render(request, 'index.html', {})

    elif request.method == "POST":
        user_nickname = request.POST.get("user-nickname")
        user_nickname = "Anonymous" if not user_nickname else user_nickname

        return render(request, 'room.html', {
            'room_name_json': mark_safe(json.dumps(room_name)),
            'user_nickname_json': mark_safe(json.dumps(user_nickname))
        })

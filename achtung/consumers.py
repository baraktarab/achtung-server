from channels.generic.websocket import AsyncWebsocketConsumer, WebsocketConsumer
import json
from achtung import consts

user_cars = {}
available_cars = {}


class GameControlConsumer(WebsocketConsumer):

    def connect(self):

        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.nickname = self.scope['url_route']['kwargs']['nickname']

        self.is_car = False

        # Connect car
        if self.room_name == consts.CARS_ROOM_NAME:
            self.is_car = True
            self.user = None
            self.room_group_name = consts.CARS_ROOM_NAME

            self.channel_layer.group_add(
                self.room_group_name,
                self.channel_name
            )

            self.accept()
            available_cars[self.channel_name] = self
            print("Car '{nickname}' Connected successfully".format(nickname=self.nickname))

        print("Available Cars - {available_cars}".format(available_cars=available_cars))

        # Connect user
        if not self.is_car:
            self.room_group_name = 'session_%s' % self.room_name

            self.car = None

            room_group_size = len(self.channel_layer.groups.get(self.room_group_name, []))

            if room_group_size < consts.MAX_IN_ROOM and len(available_cars) > 0:
                # Connect the user
                # Join user to group
                self.channel_layer.group_add(
                    self.room_group_name,
                    self.channel_name
                )

                self.car_name, self.car = available_cars.popitem()
                user_cars[self] = self.car
                self.car.user = self
                self.accept()

                self.send(text_data=json.dumps({
                    'message': "You connect successfully to the game\n"
                               "Your car is {car_name}".format(car_name=self.car.nickname)
                }))

            else:
                # Disconnect the user
                self.accept()

                self.send(text_data=json.dumps({
                    'message': "The room is full or there is not available cars"
                }))

                self.close()

    def disconnect(self, close_code=1001):
        if self.is_car:
            if self.user:
                self.user.car = None
                self.user.send_msg("Your car disconnected, Reconnect to continue")
                self.user.close()

            try:
                available_cars.pop(self)
            except KeyError:
                pass

        else:
            if user_cars.get(self):
                user_cars.pop(self)

            if self.car:
                available_cars[self.car_name] = self.car

        # Leave room group
        self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        if not self.is_car:
            if message == "L":
                self.car.send_msg("L")
                print("{room_name}: {name} - Move Left".format(room_name=self.room_name, name=self.nickname))

            elif message == "R":
                self.car.send_msg("R")
                print("{room_name}: {name} - Move Right".format(room_name=self.room_name, name=self.nickname))

            # # Send message to room group
            # self.channel_layer.group_send(
            #     self.room_group_name,
            #     {
            #         'type': 'chat_message',
            #         'message': message
            #     }
            # )

        else:
            if message == "Bad":
                pass

    def send_msg(self, msg):
        self.send(text_data=json.dumps({
            'message': msg
        }))

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))
